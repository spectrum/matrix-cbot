# mcbot

License: GPL 2.0

Copyright (C) 2020 kernel-space.org

A simple yet powerful matrix C bot with matrix-protocol level C library.

## Introduction

This bot has been developed in pure C, divided in high level bot code, plus
an additional middle-level library that implements all the basic matrix-protocol
functionalities, using additional libraries libolm, libcurl and libjson-c.

## Build

make clean
make

## Run

Setup a new  config file

```
nano ~/.mcbotrc

username "@mybot:matrix.org"
password "Trullallerolerolero"
server "https://matrix.org"
room "#test-mybot:matrix.org"

```

To execute it run

```
./mcbot
```

## Matrix protocol details

[End-to-End Encryption implementation guide](https://matrix.org/docs/guides/end-to-end-encryption-implementation-guide)

End-to-end encryption in Matrix is based on the Olm and Megolm cryptographic ratchets. They are provided by [libolm](https://github.com/louib/olm/blob/master).


### Involved keys

#### Device

**ed25519**: device identity key-pair, public-key cryptographic system for signing messages. In Matrix, each device has an Ed25519 key pair which serves to identify that device.

**curve25519**: device one-time key pairs, needed to establish a session between alice and bob, at session end, key must be dicarded and a new one-time pair needs to be used..

#### Megolm encryption keys

The Megolm key is used to encrypt group messages.

**ed25519**: megolm key-apir
