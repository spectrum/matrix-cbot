/*
 * This file is part of the matrix-cbot framework
 *    (https://codeberg.org/spectrum/matrix-cbot).
 *
 * Copyright (C) 2020 Kernelspace <info@kernel-space.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "parser.h"
#include "debug.h"

#include <string.h>

#define MAX_FIELD	256

static const json_object *parser_find_object(json_object *obj,
					     char *attr, char *rval);

json_object *parser_get_root_obj(void *ptr)
{
	return json_tokener_parse(ptr);
}

/*
 * parser_get_next_attr() - Processig a json path
 *
 * Processing a json path in the form of
 * attr."attr".attr,
 * returning the next attribute and cleaning it away for the next fetch.
 *
 * Return: 0 = success, 1 = last, -1 = error
 */
static int parser_get_next_attr(char *path, char *attr)
{
	char *p;
	int len;

	/* Process braket content, if the case */
	if (*path == '\"') {
		p = strchr(path + 1, '\"');
		if (p) {
			len = p - path - 1;
			strncpy(attr, path + 1, len);
			attr[len] = 0;
			if (!*++p) {
				/* Remove away brackets on last item */
				strcpy(path, attr);
				return 1;
			}
		} else
			/* Path error */
			return -1;
	} else {
		p = strchr(path, '.');
		if (p) {
			len = p - path;
			strncpy(attr, path, len);
			attr[len] = 0;
		} else
			return 1;
	}
	/* Advance */
	if (*(p + 1)) {
		len = strlen(++p);
		memmove(path, p, len);
		path[len] = 0;
	}

	return 0;
}

static const json_object *parser_scroll_array(json_object *jobj,
					      char *attr, char *rval)
{
	int i, array_len;
	const json_object *jrval;
	json_object *jvalue;
	enum json_type type;

	dbg("%s(): looking for [%s]\n", __func__, attr);

	array_len = json_object_array_length(jobj);

	for (i = 0; i < array_len; i++) {
		jvalue = json_object_array_get_idx(jobj, i);

		type = json_object_get_type(jvalue);
		switch (type) {
		case json_type_object:
			jrval = parser_find_object(jvalue, attr, rval);
			if (jrval)
				return jrval;
			break;
		case json_type_string: {
			const json_object *key;
			const json_object *val;
			struct lh_table *jstr;

			jstr = json_object_get_object(jvalue);

			/* Sometimes hash table is not available */
			if (!jstr)
				continue;

			key = json_object_get_object(jvalue)->head->k;
			val = json_object_get_object(jvalue)->head->v;

			if (strcmp((char *)key, attr) == 0) {
				const char *str = json_object_get_string(
							(json_object *)val);
				strcpy(rval, str);
				return (json_object *)val;
			}
			break;
		}
		default:
			dbg("json_type_xxx\n");
			break;
		}
	}

	return 0;
}

json_object *parser_get_next_array_object(json_object *jobj)
{
	static int array_pos = -1;
	int i, array_len;
	enum json_type type;

	if (json_object_get_type(jobj) != json_type_array) {
		dbg("%s(): not an array !\n", __func__);
		return NULL;
	}

	if (array_pos == -1)
		array_pos = 0;

	array_len = json_object_array_length(jobj);

	dbg("%s(): array_len %d\n", __func__, array_len);

	for (i = array_pos; i < array_len; i++) {
		json_object *jitem = json_object_array_get_idx(jobj, i);

		type = json_object_get_type(jitem);
		if (type == json_type_object) {
			array_pos = i + 1;
			return jitem;
		}
	}

	array_pos = -1;

	return NULL;
}

/*
 * Json is organized as attribute-value pairs, nested.
 *
 * Value data types:
 *
 * attribute : { object }
 * attribute : [ array ]
 * attribute : string
 * attribute : number (signed decimal, or E notation)
 * attribute : boolean
 * attribute : null
 *
 */
static const json_object *parser_find_object(json_object *obj,
					     char *attr, char *rval)
{
	enum json_type type;
	const json_object *jret;
	char *p;
	int clen = 0, rc;

	dbg("%s(): obj {%p}\n", __func__, (void *)obj);

	type = json_object_get_type(obj);
	if (type == json_type_array)
		return parser_scroll_array(obj, attr, rval);

	/* Is there a jolly char in the attr to find ? */
	p = strchr(attr, '*');
	if (p)
		clen = p - attr;

	/*
	 * Note: json_object_object_foreach only parses type object,
	 * or it will segfault.
	 */
	json_object_object_foreach(obj, key, val) {

		type = json_object_get_type(val);

		switch (type) {
		case json_type_object:
			dbg("%s(): object [%s]\n", __func__, (char *)key);
			rc = (clen) ? strncmp(key, attr, clen) :
				      strcmp(key, attr);

			if (rc == 0) {
				json_object *jobj_nxt;

				if (json_object_object_get_ex(obj, key,
							      &jobj_nxt))
					return jobj_nxt;
			}
			jret = parser_find_object(val, attr, rval);
			if (jret)
				return jret;
			break;
		case json_type_string:
			dbg("%s(): string [%s]\n", __func__, key);
			if (strcmp(key, attr) == 0) {
				const char *str = json_object_get_string(
							(json_object *)val);
				if (rval)
					strcpy(rval, str);
				return (json_object *)val;
			}
			break;
		case json_type_array: {
			dbg("%s(): array [%s]\n", __func__, key);
			if (strcmp(key, attr) == 0)
				return val;

			jret = parser_scroll_array(val, attr, rval);
			if (jret)
				return jret;
			}
			break;
		default:
			/* Int, boolean null */
			break;
		}
	}

	return 0;
}

const json_object *parser_get_object_attr_value(json_object *obj, char *attr,
					   char *str)
{
	return parser_find_object(obj, attr, str);
}

/*
 * Traverse a json path as a.b.c returning last object (c)
 */
static const json_object *parser_select_object(json_object *obj, char *path)
{
	char attr[MAX_FIELD];
	char vpath[MAX_PATH];

	/* To allow hardcodecd (.text) path */
	strcpy(vpath, path);

	for (;;) {
		if (parser_get_next_attr(vpath, attr)) {
			dbg("%s(): last in path [%s]\n", __func__, vpath);
			/* Now look for our attr  value */
			return parser_find_object(obj, vpath, 0);
		}
		dbg("%s(): next in path [%s]\n", __func__, attr);
		obj = (json_object *)parser_find_object(obj, attr, 0);
		if (!obj)
			return NULL;
	}

	return NULL;
}

json_object *parser_get_object(json_object *jobj, char *path)
{
	return (json_object *)parser_select_object(jobj, path);
}

int parser_get_object_value(json_object *obj, char *path, char *str)
{
	json_object *target;

	target = (json_object *)parser_select_object(obj, path);

	if (target) {
		const char *value;

		value = json_object_get_string(target);
		if (value)
			strcpy(str, value);

		return 0;
	}

	return 1;
}

json_object *parser_get_next_child(json_object *jobj, char *key)
{
	static struct lh_entry *cur;
	static int cnt;
	json_object *rval;

	/* End of list, reset */
	if (cur == (void *)-1) {
		cur = 0;
		return NULL;
	}

	if (!cur) {
		cnt = json_object_get_object(jobj)->count;
		cur = json_object_get_object(jobj)->head;
	}

	strcpy(key, cur->k);
	rval = (json_object *)cur->v;

	cur = cur->next;
	cnt--;

	if (!cnt)
		cur = (void *)-1;

	return rval;
}

char *parser_get_str_key(json_object *jobj)
{
	return (char *)json_object_get_object(jobj)->head->k;
}


