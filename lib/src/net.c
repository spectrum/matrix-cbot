/*
 * This file is part of the matrix-cbot framework
 *    (https://codeberg.org/spectrum/matrix-cbot).
 *
 * Copyright (C) 2020 Kernelspace <info@kernel-space.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
#include <curl/curl.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include "debug.h"
#include "parser.h"
#include "net.h"

#define MAX_URL		1024
#define MAX_POST	4096
#define MAX_TXID	32
#define MAX_PUT		MAX_POST
#define MAX_PATH	512
#define MAX_REPLY	(1024 * 32)
#define MAX_REPLY_MSGS	(1024 * 32)
#define MAX_SERVERS	16
#define MAX_NXT_BRANCH	128
#define MAX_JSON	4096
#define min(a, b)	(((a) < (b))?(a):(b))

struct context {
	CURL *curl;
	int next_idx;
};

struct server {
	int idx;
	char url[MAX_URL];
	char token[MAX_TOKEN];
};

struct reply_data {
	char *data;
	int len;
	int max_size;
};

static char rfc3986[256];
static char html5[256];
static char encoded[MAX_URL];

static char request[MAX_URL];
static char reply[MAX_REPLY];

static struct context *ctx;
static struct server servers[MAX_SERVERS];

static int net_gen_txid(void)
{
	static unsigned int txid;

	return ++txid;
}

static void net_url_encoder_rfc_tables_init(void)
{
	int i;

	for (i = 0; i < 256; i++) {
		rfc3986[i] = isalnum(i) || i == '~' ||
				i == '-' || i == '.' ||
				i == '_' || i == '?' || i == '='
				? i : 0;
		html5[i] = isalnum(i) || i == '*' || i == '-' ||
				i == '.' || i == '_'
				? i : (i == ' ') ? '+' : 0;
	}
}

/*
 * Caller responsible for memory
 */
static void net_url_encode(const char *s, char *enc, char *tb)
{
	for (; *s; s++) {
		int c = (int)*s;

		if (tb[c])
			sprintf(enc, "%c", tb[c]);
		 else
			sprintf(enc, "%%%02X", *s);

		while (*++enc)
			;
	}
}

static struct server *net_get_server(int server)
{
	if (server >= MAX_SERVERS)
		return NULL;

	return &servers[server];
}

static size_t net_get_reply(void *ptr, size_t size, size_t nmemb, void *data)
{
	struct reply_data *rd = (struct reply_data *)data;
	int tot_size = size * nmemb;
	int len;

	len = min(tot_size, (rd->max_size - rd->len));

	strncpy(&rd->data[rd->len], ptr, len);

	rd->len += len;
	rd->data[rd->len] = 0;

	return tot_size;
}

static int net_curl_get(char *url, struct reply_data *rd)
{
	int rval;
	struct curl_slist *slist1;

	slist1 = NULL;
	slist1 = curl_slist_append(slist1, "Accept: application/json");

	ctx->curl = curl_easy_init();
	curl_easy_setopt(ctx->curl, CURLOPT_BUFFERSIZE, 102400L);
	curl_easy_setopt(ctx->curl, CURLOPT_URL, url);
	curl_easy_setopt(ctx->curl, CURLOPT_NOPROGRESS, 1L);
	curl_easy_setopt(ctx->curl, CURLOPT_HTTPHEADER, slist1);
	curl_easy_setopt(ctx->curl, CURLOPT_MAXREDIRS, 50L);
	curl_easy_setopt(ctx->curl, CURLOPT_WRITEDATA, rd);
	curl_easy_setopt(ctx->curl, CURLOPT_WRITEFUNCTION, net_get_reply);
	curl_easy_setopt(ctx->curl, CURLOPT_HTTP_VERSION, (long)CURL_HTTP_VERSION_2TLS);
	curl_easy_setopt(ctx->curl, CURLOPT_CUSTOMREQUEST, "GET");
	curl_easy_setopt(ctx->curl, CURLOPT_FTP_SKIP_PASV_IP, 1L);
	curl_easy_setopt(ctx->curl, CURLOPT_TCP_KEEPALIVE, 1L);

	/* Perform the request, res will get the return code */
	rval = curl_easy_perform(ctx->curl);

	if (rval != CURLE_OK) {
		printf("err: %s\n", curl_easy_strerror(rval));
		return -2;
	}

	return 0;
}

static int net_curl_post_json(char *url, char *json, struct reply_data *rd)
{
	int rval;
	struct curl_slist *headers = NULL;

	curl_easy_setopt(ctx->curl, CURLOPT_URL, url);
	curl_easy_setopt(ctx->curl, CURLOPT_SSL_VERIFYPEER, 0L);

	headers = curl_slist_append(headers, "Accept: application/json");
	headers = curl_slist_append(headers, "Content-Type: application/json");
	headers = curl_slist_append(headers, "charset: utf-8");

	curl_easy_setopt(ctx->curl, CURLOPT_HTTPHEADER, headers);
	curl_easy_setopt(ctx->curl, CURLOPT_CUSTOMREQUEST, "POST");

	if (json)
		curl_easy_setopt(ctx->curl, CURLOPT_POSTFIELDS, json);

	curl_easy_setopt(ctx->curl, CURLOPT_WRITEDATA, rd);
	curl_easy_setopt(ctx->curl, CURLOPT_WRITEFUNCTION, net_get_reply);

	/* Perform the request, res will get the return code */
	rval = curl_easy_perform(ctx->curl);
	if (rval != CURLE_OK) {
		printf("err: %s\n", curl_easy_strerror(rval));
		return -2;
	}

	return 0;
}

static int net_curl_put_json(char *url, char *json, struct reply_data *rd)
{
	int rval;
	struct curl_slist *headers = NULL;

	curl_easy_setopt(ctx->curl, CURLOPT_URL, url);
	curl_easy_setopt(ctx->curl, CURLOPT_SSL_VERIFYPEER, 0L);

	headers = curl_slist_append(headers, "Accept: application/json");
	headers = curl_slist_append(headers, "Content-Type: application/json");
	headers = curl_slist_append(headers, "charset: utf-8");

	curl_easy_setopt(ctx->curl, CURLOPT_HTTPHEADER, headers);
	curl_easy_setopt(ctx->curl, CURLOPT_CUSTOMREQUEST, "PUT");

	curl_easy_setopt(ctx->curl, CURLOPT_WRITEDATA, rd);
	curl_easy_setopt(ctx->curl, CURLOPT_WRITEFUNCTION, net_get_reply);

	curl_easy_setopt(ctx->curl, CURLOPT_POSTFIELDS, json);

	/* Perform the request, res will get the return code */
	rval = curl_easy_perform(ctx->curl);
	if (rval != CURLE_OK) {
		printf("err: %s\n", curl_easy_strerror(rval));
		return -2;
	}

	return 0;
}

int net_server_login(char *server_url, char *username, char *password,
							char *device_id)
{
	char post_data[MAX_POST];
	struct server *s;
	struct reply_data rd = {reply, 0, MAX_REPLY};

	s = net_get_server(ctx->next_idx);
	if (!s)
		return 1;

	sprintf(post_data,
		"{ \"type\": \"m.login.password\", "
		  "\"user\": \"%s\", "
		  "\"password\": \"%s\" "
		"}",
		username,
		password);

	strncat(s->url, server_url, MAX_URL);

	strncpy(request, s->url, MAX_URL);
	strcat(request, "/_matrix/client/r0/login");

	if (net_curl_post_json(request, post_data, &rd) == 0) {
		json_object *jobj = parser_get_root_obj(rd.data);

		parser_get_object_value(jobj, "device_id", device_id);

		if (!parser_get_object_value(jobj, "access_token", s->token)) {
			ctx->next_idx++;
			return 0;
		}
	}
	return 1;
}

static int net_get_room_id(struct server *s, char *room, char *room_id)
{
	struct reply_data rd = {reply, 0, MAX_REPLY};

	strcpy(request, s->url);
	strcat(request, "/_matrix/client/r0/directory/room/");

	net_url_encode(room, encoded, rfc3986);
	strcat(request, encoded);

	if (net_curl_get(request, &rd) == 0) {
		json_object *jobj = parser_get_root_obj(rd.data);

		if (!parser_get_object_value(jobj, "room_id", room_id))
			return 0;
	}

	return 1;
}

int net_get_room_members(int server, struct room_info *ri)
{
	struct server *s;
	char path[MAX_PATH];
	struct reply_data rd = {reply, 0, MAX_REPLY};

	s = net_get_server(server);
	if (!s)
		return 1;

	strncpy(request, s->url, MAX_URL);
	sprintf(path, "/_matrix/client/r0/rooms/%s/members", ri->room_id);
	strcat(request, path);

	/* No /field to encode here */
	strcat(request, "?access_token=");
	strcat(request, s->token);
	strcat(request, "&timeout=30000");

	if (net_curl_get(request, &rd) == 0) {
		int i;
		json_object *jobj, *jobj_next;

		/* get members */
		jobj = parser_get_root_obj(rd.data);
		jobj = parser_get_object(jobj, "chunk");

		if (!jobj)
			return 1;

		for (i = 0 ;; ++i) {
			jobj_next = parser_get_next_array_object(jobj);
			if (!jobj_next)
				break;

			parser_get_object_value(jobj_next, "sender",
						ri->users[i].id);
		}
	}

	return 0;
}

int net_query_device_keys(int server, struct user *usr)
{
	struct server *s;
	char post_data[MAX_POST];
	struct reply_data rd = {reply, 0, MAX_REPLY};

	sprintf(post_data,
		"{ \"timeout\": 10000, "
		  "\"device_keys\": {\"%s\": [] }, "
		  "\"token\": \"string\" "
		"}",
		usr->id);

	s = net_get_server(server);
	if (!s)
		return 1;

	strncpy(request, s->url, MAX_URL);
	strcat(request, "/_matrix/client/r0/keys/query");
	strcat(request, "?access_token=");
	strcat(request, s->token);
	strcat(request, "&timeout=30000");

	if (net_curl_post_json(request, post_data, &rd) == 0) {
		json_object *jobj, *jdev;
		char path[MAX_PATH];
		int i;

		sprintf(path, "device_keys.\"%s\"", usr->id);
		jobj = parser_get_root_obj(rd.data);

		/* We are in the father device object */
		jobj = parser_get_object(jobj, path);
		if (!jobj)
			return 1;

		for (i = 0; i < MAX_DEVICES; ++i) {
			jdev = parser_get_next_child(jobj, usr->devs[i].name);
			if (!jdev)
				break;

			sprintf(path, "keys.ed25519:%s", usr->devs[i].name);
			parser_get_object_value(jdev, path,
						usr->devs[i].ed25519);

			sprintf(path, "unsigned.device_display_name");
			parser_get_object_value(jdev, path, usr->devs[i].info);
		}
	}

	return 0;
}

int net_sync_messages(int server, struct room_info *ri,
		      tod_msg tm,
		      callback_func cf
		      )
{
	struct server *s;
	static char next_batch[MAX_NXT_BRANCH];
	char reply[MAX_REPLY_MSGS];
	struct reply_data rd = {reply, 0, MAX_REPLY_MSGS};
	int arr_len;

	s = net_get_server(server);
	if (!s)
		return 1;

	strncpy(request, s->url, MAX_URL);
	strcat(request, "/_matrix/client/r0/sync");
	strcat(request, "?access_token=");
	strcat(request, s->token);
	strcat(request, "&timeout=30000");
	strcat(request, "&filter=0");


	if (*next_batch) {
		strcat(request, "&since=");
		strcat(request, next_batch);
	}

	if (net_curl_get(request, &rd) == 0) {
		char path[MAX_PATH];
		char str[512];
		struct evt_encrypted e;
		int i;
		struct to_device *to_device_evts;
		json_object *jobj, *jnext;
		json_object *tmp;
		json_object *jobj_to_device;
		struct evt_encrypted *evt;

		printf("sync: %s\n", rd.data);

		jobj = parser_get_root_obj(rd.data);

		if (parser_get_object_value(jobj, "next_batch", next_batch))
			return 1;
		size_t len = 0;

		jobj_to_device = parser_get_object(jobj,
						"to_device.events");
		/*Array of events, must count how much of them*/
		len = json_object_array_length(jobj_to_device);
		to_device_evts = malloc(len*sizeof(struct to_device));
		/*Filling all the structures*/
		for (i = 0; i < len; i++) {
			tmp = json_object_array_get_idx(jobj_to_device, i);
			/*Now we have an event*/
			parser_get_object_value(tmp, "type",
						to_device_evts[i].type);
			parser_get_object_value(tmp, "sender",
						to_device_evts[i].sender);
			if (strcmp(to_device_evts[i].type,
				   "m.room.encrypted") == 0) {
				/*Then we allocate a m.room.encrypted*/
				evt = malloc(sizeof(struct evt_encrypted));
				to_device_evts[i].content = evt;
				/*Now we fill that event*/
				json_object *encr_evt
				= parser_get_object(jobj_to_device, "content");
				parser_get_object_value(encr_evt, "algorithm",
							evt->algorithm);
				parser_get_object_value(encr_evt, "sender_key",
							evt->sender_key);
				parser_get_object_value(encr_evt, "ciphertext",
							evt->ciphertext);
				/* for each to device event fire a callback
				 * to handle them, we care only about those
				 * who carry m.room_key events for the moment.
				 */
				tm(ri, &to_device_evts[i]);
			}
		}

		/* Getting "events" father obj */
		sprintf(path,
			"rooms.join.\"%s\".timeline.events",
			ri->room_id);

		jobj = parser_get_object(jobj, path);
		arr_len = json_object_array_length(jobj);
		for (i = 0 ; i < arr_len; i++) {
			jnext = json_object_array_get_idx(jobj,i);
			if (!jnext)
				break;
			cf(jnext, ri);
		}

		return 0;
	}

	return 1;
}
int net_claim_one_time_key(int server, char *user_id,
			   char *device_id, char *one_time_key)
{
	struct server *s;
	char post_data[MAX_POST];
	struct reply_data rd = {reply, 0, MAX_REPLY};

	s = net_get_server(server);
	if (!s)
		return 1;

	sprintf(post_data,
		"{"
		"\"timeout\": 10000,\"one_time_keys\": {"
		"\"%s\": {"
			"\"%s\": \"signed_curve25519\" }}}",
		user_id, device_id);
	strncpy(request, s->url, MAX_URL);
	strcat(request, "/_matrix/client/r0/keys/claim");
	strcat(request, "?access_token=");
	strcat(request, s->token);
	strcat(request, "&timeout=30000");

	if (net_curl_post_json(request, post_data, &rd) == 0) {
		char path[MAX_PATH];
		json_object *jobj;

		jobj = parser_get_root_obj(rd.data);

		sprintf(path,
			"one_time_keys.\"%s\".%s.signed_curve25519*.key",
			user_id, device_id);

		if (!parser_get_object_value(jobj, path, one_time_key))
			return 0;
	}
	return 1;
}

int net_request_megolm_keys(struct room_info *ri,
			    char *sender_key,
			    char *session_id,
			    char *random_string,
			    char *device_id,
			    char *payload)
{

	sprintf(payload,
		"{"
			"\"content\": {"
				"\"action\" : \"request\",\"body\": {"
					"\"algorithm\" :\"m.megolm.v1.aes-sha2\","
					"\"room_id\": \"%s\",\"sender_key\": \"%s\","
					"\"session_id\": \"%s\"},"
				"\"request_id\" : \"%s\",\"requesting_device_id\": \"%s\""
			"},\"type\": \"m.room_key_request\""
		"}",
	 ri->room_id, sender_key, session_id,
	 random_string, device_id);

	return 0;
}

/**
 * @abstract use this function to send our keys
 * to the server and make them available to others
 * so that others can encrypt the message with
 * our keys and we are able to decrypt them.
 * @param device_keys : retrieved from olm_account_identity_keys()
 * @param one_time_keys : retrieved from olm_account_generate_one_time_keys()
 * @param user_id : user_id such as @mcbot:matrix.org
 * @param device_id : such as "ADFHBASAD"
 * @param signatures : map from user_id to identity key for the device
 */
int net_upload_keys(char *device_keys,
		    char *one_time_keys,
		    char *user_id,
		    char *device_id,
		    int server)
{
	char payload[MAX_JSON] = {0};
	char signed_payload[2*MAX_JSON] = { 0 };
	struct server *s;
	struct reply_data rd = {reply, 0, MAX_REPLY};

	s = net_get_server(server);

	/*WARNING don't add spaces or format, because this is CANONICAL JSON*/
	sprintf(payload,
		"{"
			"\"algorithms\":[\"m.olm.v1.curve25519-aes-sha2\","
				"\"m.megolm.v1.aes-sha2\"],"
			"\"device_id\":\"%s\",\"keys\":%s,"
			"\"user_id\":\"%s\"}",
	 device_id, device_keys, user_id
	);
	cipher_sign_payload(payload, strlen(payload));
	sprintf(signed_payload,
		"{"
			"\"device_keys\":{"
				"\"algorithms\":[\"m.olm.v1.curve25519-aes-sha2\","
					"\"m.megolm.v1.aes-sha2\"],"
				"\"device_id\":\"%s\",\"keys\":%s,"
				"\"signatures\":{"
					"\"%s\":{"
						"\"ed25519:%s\":\"%s\"}"
				"},\"user_id\":\"%s\""
			"},\"one_time_keys\":%s"
		"}",
	 device_id, device_keys,
	 user_id, device_id, payload, user_id,
	 one_time_keys
	);
	strncpy(request, s->url, MAX_URL);
	strcat(request, "/_matrix/client/r0/keys/upload");
	strcat(request, "?access_token=");
	strcat(request, s->token);
	strcat(request, "&timeout=30000");
	net_curl_post_json(request, signed_payload, &rd);

	return 0;
}
/**
 * Gets curve25519 for the device
 */
int net_query_curve_key(int server, char *user_id,
			   char *device_id, char *curve_key)
{
	struct server *s;
	char post_data[MAX_POST];
	struct reply_data rd = {reply, 0, MAX_REPLY};

	s = net_get_server(server);
	if (!s)
		return 1;

	sprintf(post_data,
		"{"
		"\"timeout\": 10000,\"device_keys\": {"
				"\"%s\": [\"%s\"]" //list of devices, empty for all devices
				"}}",
	 user_id, device_id);
	strncpy(request, s->url, MAX_URL);
	strcat(request, "/_matrix/client/r0/keys/query");
	strcat(request, "?access_token=");
	strcat(request, s->token);
	strcat(request, "&timeout=30000");

	if (net_curl_post_json(request, post_data, &rd) == 0) {
		char path[MAX_PATH];
		json_object *jobj;


		jobj = parser_get_root_obj(rd.data);

		sprintf(path,
			"device_keys.\"%s\".%s.keys.curve25519:%s",
			user_id, device_id, device_id);

		if (!parser_get_object_value(jobj, path, curve_key))
			return 0;
	}
	return 1;
}
/**
 * @abstract :	core function for encryption, builds up the payload to share
 *		megolm session
 * @param ri : room_informations needed for room_id
 * @param megolm_session : needed for encryption with olm
 * @param d : device recipient of the megolm keys we are sending
 * @param u : user recipient of the megolm keys we are sending
 * @param device_curve_key : curve25519 key of the recipient device
 * @param our_device_keys : our keys needed for encryption and signature
 * @param finalPayload : will contain the final payload after all the packaging is done
 */
int net_setup_megolm_key_sharing_payload(struct room_info *ri,
				      struct olm_session *megolm_session,
				      struct device *d,
				      struct user *u,
				      struct device_keys *our_device_keys,
				      char *finalPayload)
{
	char keys[MAX_JSON] = { 0 };
	char recipEd25519[MAX_JSON] = { 0 };
	char ourEd25519[MAX_JSON] = { 0 };
	char payload[MAX_JSON] = { 0 };
	char ciphertext[MAX_JSON] = { 0 };
	char keyJson[MAX_JSON] = { 0 };
	char ciphertextJson[MAX_JSON] = { 0 };

	// create the content for m.room_key and stores it in keys
	net_setup_m_room_key(keys, ri, megolm_session->ed25519,
			     megolm_session->session_key);
	//create recipient ed25519 (identification key) as a JSON object
	sprintf(recipEd25519, "{"
				"\"ed25519\": \"%s\"}",
		d->ed25519
		);
	//create our ed25519 (identification key) as a JSON object
	sprintf(ourEd25519, "{"
				"\"ed25519\": \"%s\"}",
		megolm_session->ed25519
		);
	// fill the payload for the m.room_key event with required informations
	sprintf(payload,
			"{"
				"\"content\": %s," //keys
				"\"sender\": \"@mcbot:matrix.org\"," //user id
				"\"recipient\": \"%s\"," //recipient id
				"\"recipient_keys\" : %s," //recipEd25519
				"\"keys\": %s," //ourEd25519
				"\"type\": \"m.room_key\"}",
		keys, u->id, recipEd25519, ourEd25519
		);
	//encrypt the payload with the olm session created with device
	cipher_encrypt_m_room_key(d->s, payload, ciphertext);
	//add ciphertext to the JSON Object with type 0 (pre key message)
	sprintf(keyJson,
			"{"
				"\"type\": 0,\"body\": \"%s\""
			"}",
		ciphertext);
	// package the previous object into a map associating curve key of recipient device with ciphertext
	// device is receiving an encrypted payload containing our megolm keys
	sprintf(ciphertextJson,
			"{"
				"\"%s\" : %s" // curve25519 : keyJson
			"}",
		d->curve25519, keyJson
		);
	//wrap everything as an m.room.encrypted event
	sprintf(finalPayload,
			"{"
				"\"algorithm\": \"m.olm.v1.curve25519-aes-sha2\",\"sender_key\": \"%s\","
				 //curve25519
				"\"ciphertext\": %s" //ciphertextJson
			"}",
			our_device_keys->curve25519, ciphertextJson
		);
	return 0;
}
/**
 * This function has the purpose to create the_payload cited here
 * https://matrix.org/docs/spec/client_server/r0.4.0#m-room-key
 */
int net_setup_m_room_key(char *output, struct room_info *ri,
				  char *session_id,
				  char *session_key)
{

	sprintf(output,
		"{"
			"\"algorithm\": \"m.megolm.v1.aes-sha2\",\"room_id\": \"%s\","
			"\"session_id\": \"%s\",\"session_key\": \"%s\""
		"}",
	ri->room_id, session_id, session_key
	);
	return 0;
}

/**
 * Use this to send messages encrypted to the room
 */
int net_setup_m_room_message(
			      char *room_id,
			      char *text_message,
			      char *payload)
{
	sprintf(payload,
		"{"
		"\"type\" : \"m.room.message\",\"content\" : {"
			"\"msgtype\" : \"m.text\","
			"\"format\": \"org.matrix.custom.html\","
			"\"formatted_body\": \"%s\","
			"\"body\" : \"%s\""
			"},\"room_id\" : \"%s\""
		"}",
	 text_message,
	 text_message, room_id);
	return 0;
}
/**
 * Package to send message
 */
int net_setup_text_package(
			      char *encrypted_payload,
			      char *sender_key,
			      char *session_id,
			      char *device_id,
			      char *package)
{
	sprintf(package,
		"{"
			"\"algorithm\" : \"m.megolm.v1.aes-sha2\",\"sender_key\" : \"%s\","
			"\"ciphertext\" : \"%s\",\"session_id\" : \"%s\","
			"\"device_id\" : \"%s\"}",
	sender_key, encrypted_payload, session_id, device_id);
	return 0;
}

int net_send_text_message(char *text,
			  struct room_info *ri,
			  struct olm_session *megolm_session,
			  int server,
			  struct device_keys *dev_keys,
			  char *device_id)
{
	char payload[MAX_JSON] = { 0 };
	char encrypted[MAX_JSON] = { 0 };
	char package[MAX_JSON] = { 0 };

	net_setup_m_room_message(ri->room_id, text, payload);
	cipher_encrypt_text_message(megolm_session, payload, encrypted);
	net_setup_text_package(encrypted, dev_keys->curve25519,
					megolm_session->ed25519,
					device_id,
					package);
	net_send_m_room_encrypted(server, ri->room_id, package);
	return 0;
}
int net_send_m_room_encrypted(int server,
			      char *room_id,
			      char *payload)
{
	struct server *s;
	char txid[MAX_TXID];
	struct reply_data rd = {reply, 0, MAX_REPLY};


	s = net_get_server(server);
	if (!s)
		return 1;

	strcpy(request, s->url);
	strcat(request, "/_matrix/client/r0/rooms/");
	strcat(request, room_id);
	strcat(request, "/send/m.room.encrypted/");
	sprintf(txid, "%d", net_gen_txid());
	strcat(request, txid);
	strcat(request, "?access_token=");
	strcat(request, s->token);
	strcat(request, "&timeout=30000");


	net_curl_put_json(request, payload, &rd);
	printf("reply to message %s\n", rd.data);
	return 0;
}
/**
 * @abstract send_to_device implementation
 * @param event : m.room.encrypted,m.room_key_request, etc..
 */
int net_send_to_deivce(int server,
		       char *dest_id,
		       char *dest_device,
		       char *payload,
		       char *event)
{

	struct server *s;
	char put_data[MAX_PUT] = { 0 };
	char txid[MAX_TXID];
	struct reply_data rd = {reply, 0, MAX_REPLY};
	int ret;

	s = net_get_server(server);
	if (!s)
		return 1;

	strncpy(request, s->url, MAX_URL);
	strcat(request, "/_matrix/client/r0/sendToDevice/");
	strcat(request, event);
	strcat(request, "/");
	sprintf(txid, "%d", net_gen_txid());
	strcat(request, txid);
	strcat(request, "?access_token=");
	strcat(request, s->token);
	strcat(request, "&timeout=30000");
	printf("request %s\n", request);
	sprintf(put_data,
		"{"
		"\"messages\": {"
			"\"%s\" : {"
				"\"%s\" :%s"
				"}}"
		"}",
	 dest_id, dest_device,
	 payload
	);
	printf("put_data %s\n", put_data);
	ret = net_curl_put_json(request, put_data, &rd);
	printf("rd data %s\n", rd.data);
	if (ret != 0)
		return 1;

	return 0;
}

/**
 * Sends the public key to all devices in the room,
 * the key is stored in the ciphertext which is encrypted
 * by the initial shared secret that is a curve25519 key.
 */
int net_send_m_room_encrypted_to_device(int server,
			      char *dest_id,
			      char *dest_device,
			      char *payload
			      )
{
	struct server *s;
	char put_data[MAX_PUT] = { 0 };
	char txid[MAX_TXID];
	struct reply_data rd = {reply, 0, MAX_REPLY};
	int ret;

	s = net_get_server(server);
	if (!s)
		return 1;

	strncpy(request, s->url, MAX_URL);
	strcat(request, "/_matrix/client/r0/sendToDevice/m.room.encrypted/");
	sprintf(txid, "%d", net_gen_txid());
	strcat(request, txid);
	strcat(request, "?access_token=");
	strcat(request, s->token);
	strcat(request, "&timeout=30000");

	sprintf(put_data,
		"{"
		"\"messages\": {"
			"\"%s\" : {"
				"\"%s\" :%s"
				"}}"
		"}",
	 dest_id, dest_device,
	 payload
	);
	ret = net_curl_put_json(request, put_data, &rd);
	if (ret != 0)
		return 1;

	return 0;
}

static int net_join_room_simple(struct server *s, struct room_info *ri)
{
	char request_enc[MAX_URL];
	struct reply_data rd = {reply, 0, MAX_REPLY};

	strncpy(request, s->url, MAX_URL);
	strcat(request, "/_matrix/client/r0/join/");
	strcpy(request_enc, ri->room_id);
	strcat(request_enc, "?access_token=");
	strcat(request_enc, s->token);

	net_url_encode(request_enc, encoded, rfc3986);

	strcat(request, encoded);

	if (net_curl_post_json(request, 0, &rd) == 0)
		return 0;

	return 1;
}

struct room_info *net_join_room(int server, char *room)
{
	struct server *s;
	struct room_info *ri;

	s = net_get_server(server);
	if (!s)
		return NULL;

	s = &servers[server];

	ri = (struct room_info *)malloc(sizeof(struct room_info));

	if (!ri) {
		dbg("%s(): end of heap memory\n", __func__);
		return NULL;
	}

	memset(ri, 0, sizeof(struct room_info));

	net_get_room_id(s, room, ri->room_id);

	if (!net_join_room_simple(s, ri))
		return ri;

	return NULL;
}

int net_logout_all(int server)
{
	struct server *s = net_get_server(server);
	struct reply_data rd = {reply, 0, MAX_REPLY};

	strncpy(request, s->url, MAX_URL);
	strcat(request, "/_matrix/client/r0/logout/all");
	strcat(request, "?access_token=");
	strcat(request, s->token);

	net_curl_post_json(request, NULL, &rd);
	printf("Result for device delete %s\n", rd.data);

	return 0;
}

static void net_exit(void)
{
	curl_easy_cleanup(ctx->curl);
	curl_global_cleanup();
}

int net_init(void)
{
	curl_global_init(CURL_GLOBAL_DEFAULT);

	if (!ctx)
		ctx = (struct context *)calloc(1, sizeof(struct context));

	if (!ctx)
		return -1;

	if (ctx->curl)
		net_exit();

	net_url_encoder_rfc_tables_init();
	ctx->curl = curl_easy_init();

	return 0;
}
