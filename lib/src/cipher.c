/*
 * This file is part of the matrix-cbot framework
 *    (https://codeberg.org/spectrum/matrix-cbot).
 *
 * Copyright (C) 2020 Kernelspace <info@kernel-space.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "cipher.h"
#include "parser.h"
#include "net.h"
#include "debug.h"

#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>

struct context {
	/* Account */
	char *mem_account;
	OlmAccount *account;
	/* Megolm (group/room sessions) */
	char *mem_out_sess;
	OlmOutboundGroupSession *out_group_session;
	char *mem_in_sess;
	OlmInboundGroupSession *in_group_session;
	uint8_t session_id[MAX_SESSION_ID];
	uint8_t session_key[MAX_SESSION_KEY];
	/* Device keys, Json format */
	uint8_t *identity_keys;
	char *last_one_time_keys;
};

static struct context ctx;

#define RANDOM_BYTES_LEN	4096
#define MAX_VALUE		1024
#define MAX_MSG			4096
uint8_t random_bytes_account[RANDOM_BYTES_LEN];
uint8_t random_bytes_session[RANDOM_BYTES_LEN];
uint8_t random_bytes_temp[RANDOM_BYTES_LEN];

int cipher_get_random(void)
{
	FILE *fp = fopen("/dev/urandom", "r");

	if (fp == NULL)
		return 1;
	fread(random_bytes_session, sizeof(u_int8_t), RANDOM_BYTES_LEN, fp);
	fread(random_bytes_account, sizeof(u_int8_t), RANDOM_BYTES_LEN, fp);
	fclose(fp);
	return 0;
}

int cipher_init(struct olm_session *s)
{
	size_t s_size;

	if (cipher_get_random())
		return 1;
	/* Creating device identity keys */
	ctx.mem_account = malloc(olm_account_size());
	if (!ctx.mem_account)
		return 1;

	ctx.account = olm_account(ctx.mem_account);
	if (!ctx.account)
		return 1;

	s_size = olm_create_account_random_length(ctx.account);
	if (olm_create_account(ctx.account, random_bytes_account, s_size)
		== olm_error())
		return 1;

	s_size = olm_outbound_group_session_size();
	if (!s_size)
		return 1;

	ctx.mem_out_sess = malloc(s_size);
	if (!ctx.mem_out_sess)
		return 1;

	ctx.out_group_session = olm_outbound_group_session(ctx.mem_out_sess);
	if (!ctx.out_group_session)
		return 1;

	s_size = olm_init_outbound_group_session_random_length(
						ctx.out_group_session);
	if (olm_init_outbound_group_session(ctx.out_group_session,
					    random_bytes_session, s_size)
					    == olm_error())
		return 1;

	s_size = olm_outbound_group_session_id_length(ctx.out_group_session);
	if (olm_outbound_group_session_id(ctx.out_group_session,
					  ctx.session_id,
					  s_size) == olm_error())
		return 1;
	s_size = olm_outbound_group_session_key_length(ctx.out_group_session);
	if (olm_outbound_group_session_key(ctx.out_group_session,
					   ctx.session_key,
					   4096) == olm_error())
		return 1;

	strncpy(s->ed25519, (char *)ctx.session_id, MAX_SESSION_ID);
	strncpy(s->session_key, (char *)ctx.session_key, MAX_SESSION_KEY);

	s_size = olm_inbound_group_session_size();

	ctx.mem_in_sess = malloc(s_size);
	if (!s_size)
		return 1;
	ctx.in_group_session = olm_inbound_group_session(ctx.mem_in_sess);
	if (!ctx.in_group_session)
		return 1;

	if (olm_init_inbound_group_session(ctx.in_group_session,
					ctx.session_key,
					strlen((char *)ctx.session_key))
					== olm_error())
		return 1;
	s->outbound = ctx.out_group_session;

	return 0;
}

int cipher_create_device_keys(struct device_keys *dev_keys)
{
	int s_size;
	char *p, *q;

	/* Device keys */
	s_size = olm_account_identity_keys_length(ctx.account);
	ctx.identity_keys = calloc(s_size, sizeof(uint8_t));

	/* Here base64 of device identity keys are created */
	if (olm_account_identity_keys(ctx.account, ctx.identity_keys, s_size)
		== olm_error())
		return 1;

	p = strstr((char *)ctx.identity_keys, ":\"");

	if (!p)
		return 1;

	q = strstr(p + 2, "\"");

	if (!q)
		return 1;

	strncpy(dev_keys->curve25519, p + 2, q - p - 2);

	p = strstr(q + 1, ":\"");

	if (!p)
		return 1;

	q = strstr(p + 2, "\"");

	if (!q)
		return 1;
	strncpy(dev_keys->ed25519, p + 2, q - p - 2);

	return 0;
}

int cipher_decrypt_group(uint8_t *message,
			  uint8_t *plaintext, uint32_t *msg_idx,
			  OlmInboundGroupSession *inb)
{
	int size, len;
	char msg_copy[MAX_MSG];

	len = strlen((char *)message);
	strncpy(msg_copy, (char *)message, len);

	size = olm_group_decrypt_max_plaintext_length(inb,
						      (uint8_t *)msg_copy, len);

	dbg("olm_group_decrypt size %d\n", size);

	if (olm_group_decrypt(inb,
			  message, strlen((char *)message),
			  plaintext, size,
			  msg_idx) == olm_error()) {
		printf("error during decrypt %s\n",
		       olm_inbound_group_session_last_error(inb));
		return 1;
	}
	return 0;
}

int cipher_create_outbound_session(char *their_id_key,
				    char *their_one_time_key,
				    struct olm_session *s)
{
	int len;
	char *mem;
	int i;

	mem = malloc(olm_session_size());

	if (!mem)
		return 1;

	s->session = olm_session(mem);
	if (!s->session)
		return 1;

	for (i = 0; i < RANDOM_BYTES_LEN; ++i)
		random_bytes_temp[i] = rand();

	len = olm_create_outbound_session_random_length(s->session);

	dbg("%s %d\n", __func__, len);

	/*initialize inbound session to NULL*/
	s->inboud = NULL;

	if (olm_create_outbound_session(s->session,
					   ctx.account,
					   their_id_key,
					   strlen(their_id_key),
					   their_one_time_key,
					   strlen(their_one_time_key),
					   random_bytes_temp, len)
					== olm_error()) {
		return 1;
	}

	return 0;
}

/*
 * Using this message to send the megolm key to device
 */
int cipher_encrypt_m_room_key(struct olm_session *s,
			      char *payload, char *encrypted)
{
	int i;
	int mlen = olm_encrypt_message_length(s->session, strlen(payload));
	int len = olm_encrypt_random_length(s->session);

	for (i = 0; i < len; ++i)
		random_bytes_temp[i] = rand();

	if (olm_encrypt(s->session,
			payload, strlen(payload),
			random_bytes_temp, len,
			encrypted, mlen) == -1) {
		dbg("%s() error %d\n", __func__, olm_error());
		return 1;
	}

	return 0;
}
int cipher_encrypt_text_message(struct olm_session *s,
				char *payload,
				char *encrypted)
{

	int mlen = olm_group_encrypt_message_length(s->outbound,
						strlen(payload));

	if (olm_group_encrypt(s->outbound, payload, strlen(payload),
			      encrypted, mlen) == -1) {
		dbg("%s() error %d\n", __func__, olm_error());
		return 1;
	}
	return 0;
}

int cipher_get_identity_keys_length(void)
{

	return olm_account_identity_keys_length(ctx.account);
}

int cipher_get_identity_keys(char *identity_keys, char *device_id)
{
	/*As always we need to change the output of this*/
	char tmp[MAX_JSON];
	char ed25519[MAX_VALUE];
	char curve25519[MAX_VALUE];

	strcpy(tmp, ctx.identity_keys);
	json_object *jobj = parser_get_root_obj(tmp);

	parser_get_object_value(jobj, "curve25519", curve25519);
	parser_get_object_value(jobj, "ed25519", ed25519);
	sprintf(identity_keys, "{"
	/*Creating the payload stated in the matrix specs*/
			"\"curve25519:%s\":\"%s\","
			"\"ed25519:%s\":\"%s\""
			"}",
	 device_id, curve25519,
	 device_id, ed25519
	);
	return 0;
}
/**
 * @abstract returns a payload of generated one time keys
 * @warning the returned payload must be freed as it is
 * allocated dinamically.
 */
char *cipher_get_one_time_keys(void)
{
	char output[MAX_JSON] = { 0 };
	json_object *jobj;
	int first_object = 0;
	char value[MAX_VALUE];
	json_object_iter iter;
	char *retval = malloc(MAX_JSON);
	/*Generating one-time keys*/
	size_t nk = olm_account_max_number_of_one_time_keys(ctx.account);
	/*Pushing upstream only the half of them*/
	olm_account_generate_one_time_keys(ctx.account, nk/2,
				   random_bytes_account, RANDOM_BYTES_LEN);
	size_t otkl = olm_account_one_time_keys_length(ctx.account);

	ctx.last_one_time_keys =  malloc(sizeof(char)*otkl);

	olm_account_one_time_keys(ctx.account, ctx.last_one_time_keys, otkl);
	olm_account_mark_keys_as_published(ctx.account);
	/*We need to modify the payload obtained and
	 *add curve25519 to the normal AAAAQ identifier
	 * for the one time key, otherwise clients
	 * will reject them.
	 */
	jobj = parser_get_root_obj(ctx.last_one_time_keys);
	jobj = parser_get_object(jobj, "curve25519");

	sprintf(output, "{");
	json_object_object_foreachC(jobj, iter) {
		/*we need dont need a comma separation if first*/
		if (!first_object) {
			strcat(output, "\"curve25519:");
			first_object = 1;
		} else {
			strcat(output, ",\"curve25519:");
		}
		parser_get_object_value(jobj, iter.key, value);
		strcat(output, iter.key);
		strcat(output, "\":\"");
		strcat(output, value);
		strcat(output, "\"");
	}
	strcat(output, "}");
	strcpy(retval, output);

	return retval;
}

int cipher_sign_payload(char *payload, int payload_len)
{

	size_t len = olm_account_signature_length(ctx.account);

	char *output = calloc(len, sizeof(char));

	olm_account_sign(ctx.account, payload, payload_len,
			 output, len);
	strcpy(payload, output);

	return 0;
}
