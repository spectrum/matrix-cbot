#ifndef __cipher_h
#define __cipher_h

#include <stdint.h>
#include "net.h"
#include <olm/olm.h>

#define MAX_SESSION_ID		4096
#define MAX_SESSION_KEY		4096
#define MAX_IDENT_KEYS		1024

#define MAX_KEY_ED	64
#define MAX_KEY_CURVE	64

/*
 * Device keys, base64 encoded
 * curve25519 is the one-time key pair.
 */
struct device_keys {
	char ed25519[MAX_KEY_ED];
	char curve25519[MAX_KEY_CURVE];
};

struct olm_session {
	OlmSession *session;
	char ed25519[MAX_KEY_ED]; //session id
	char session_key[MAX_SESSION_KEY]; //session key
	OlmOutboundGroupSession *outbound;
	OlmInboundGroupSession *inboud;
	char *device_id;
};

int cipher_init(struct olm_session *s);
int cipher_create_device_keys(struct device_keys *dev_keys);
int cipher_decrypt_group(uint8_t *message,
			  uint8_t *plaintext, uint32_t *msg_idx,
			  OlmInboundGroupSession *inb);
int cipher_create_outbound_session(char *their_id_key,
				    char *their_one_time_key,
				    struct olm_session *s);
int cipher_encrypt_m_room_key(struct olm_session *s,
			      char *payload, char *encrypted);
int cipher_encrypt_text_message(struct olm_session *s,
				char *payload, char *encrypted);

int cipher_get_identity_keys(char *identity_keys, char *device_id);

int cipher_get_identity_keys_length(void);

char *cipher_get_one_time_keys(void);

int cipher_sign_payload(char *payload, int payload_len);
#endif /* __cipher_h */

