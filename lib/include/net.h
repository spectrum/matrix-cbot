#ifndef __net_h
#define __net_h
#include "cipher.h"
#include "parser.h"

#define MAX_ROOM_ID	256
#define MAX_ROOM_USERS	1024
#define MAX_DEVICES	50
#define MAX_DEVICE_NAME	16
#define MAX_DEVICE_INFO	128
#define MAX_SESSION_ID	4096
#define MAX_ALGORITHM	64
#define MAX_CIPHERTEXT	4096
#define MAX_USER_ID	64
#define MAX_KEY_LEN	64
#define MAX_TYPE	32
#define MAX_JSON	4096
#define SHORT_TEXT	64
#define MAX_MEGOLM_KEY	1024
/*
 * Session with a device
 */
struct device_session {};
struct evt_encrypted {
	char sender[MAX_USER_ID];
	char algorithm[MAX_ALGORITHM];
	char ciphertext[MAX_CIPHERTEXT];
	char device_id[MAX_DEVICE_NAME];
	char sender_key[MAX_KEY_LEN];
	char session_id[MAX_SESSION_ID];
};

struct to_device {
	char type[MAX_TYPE];
	char sender[MAX_USER_ID];
	/*Using void to cast to whatever content will be inside*/
	void *content;
};

struct m_room_member {
	char membership[SHORT_TEXT];
	char sender[SHORT_TEXT];
};
/*
 * Device information
 */
struct device {
	char name[MAX_DEVICE_NAME];
	char info[MAX_DEVICE_INFO];
	char ed25519[MAX_KEY_LEN];
	char curve25519[MAX_KEY_LEN];
	char megolm_session[MAX_MEGOLM_KEY];
	struct olm_session *s;
};

/*
 * User information
 */
struct user {
	char id[MAX_USER_ID];
	struct device devs[MAX_DEVICES];
};

/*
 * Room information
 */
struct room_info {
	char room_id[MAX_ROOM_ID];
	struct user users[MAX_ROOM_USERS];
};
struct callbacks;
typedef void (*func_msg)(struct room_info *ri, struct evt_encrypted *evt);
typedef void (*tod_msg)(struct room_info *ri, struct to_device *evt);
typedef void (*callback_func)(json_object *jobj, struct room_info *ri);
int net_init(void);
int net_server_login(char *server_url, char *username, char *password,
							char *device_id);
struct room_info *net_join_room(int server, char *room);
int net_get_room_members(int server, struct room_info *ri);
int net_query_device_keys(int server, struct user *usr);
int net_sync_messages(int server, struct room_info *ri,
			tod_msg tm,
		      callback_func cf);
int net_share_session(int server, char *room, char *user_id, char *session_id,
			char *session_key);
int net_claim_one_time_key(int server, char *user_id, char *device_id,
			   char *one_time_key);
int net_upload_keys(char *device_keys,
		    char *one_time_keys,
		    char *user_id,
		    char *device_id,
		    int server);
int net_setup_m_room_key(char *output, struct room_info *ri,
				  char *session_id,
				  char *session_key);
int net_send_m_room_encrypted_to_device(int server,
			      char *dest_id,
			      char *dest_device,
			      char *payload
			      );

int net_request_megolm_keys(struct room_info *ri, char *sender_key,
			    char *session_id,
			    char *random_string,
			    char *device_id,
			    char *payload);
int net_setup_m_room_message(
			      char *room_id,
			      char *text_message,
			      char *payload);
int net_send_m_room_encrypted(int server,
			      char *room_id,
			      char *payload);
int net_send_to_deivce(int server,
		       char *dest_id,
		       char *dest_device,
		       char *payload,
		       char *event);
int net_setup_text_package(
			      char *encrypted_payload,
			      char *sender_key,
			      char *session_id,
			      char *device_id,
			      char *package);
int net_send_text_message(char *text,
			  struct room_info *ri,
			  struct olm_session *megolm_session,
			  int server,
			  struct device_keys *dev_keys,
			  char *device_id);
int net_query_curve_key(int server, char *user_id,
			   char *device_id, char *curve_key);
int net_setup_megolm_key_sharing_payload(struct room_info *ri,
				      struct olm_session *megolm_session,
				      struct device *d,
				      struct user *u,
				      struct device_keys *our_device_keys,
				      char *finalPayload);
int net_logout_all(int server);
#endif /* __net_h */
