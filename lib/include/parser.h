#ifndef __parser_h
#define __parser_h

#define MAX_PATH  512

#include <json-c/json.h>

/**
 * Simple json parser to be used with Matrix protocol.
 *
 * It allows to retrieve values from a specified path, as
 * ob1.obj2.array.attribute
 *
 * Or, it allows to retrieve specific objects, or objects attributes
 * by attribute name.
 */

#define MAX_TOKEN	512

/**
 * parser_get_root_obj() - get root object from json string
 *
 * @ptr : root object
 *
 * RETURNS: the root object
 */
json_object *parser_get_root_obj(void *ptr);

/**
 * parser_get_object() - get object by path, from json root object
 *
 * @jobj : root object
 * @path : json path, in form of attr."attr".attr etc
 *
 * RETURNS: the requested object
 */
json_object *parser_get_object(json_object *jobj, char *path);

/**
 * parser_get_object_value() - get object value by path, from json root object
 *
 * @jobj : root object
 * @path : json path, in form of attr."attr".attr etc
 * @str: value read
 *
 * RETURNS: the requested object value
 */
int parser_get_object_value(json_object *jobj, char *path, char *str);

/**
 * parser_get_object_attr_value() - get the value of an object attribute
 *
 * @jobj : root object
 * @attr : attribute to get value
 * @str: value read
 *
 * RETURNS: the target object
 */
const json_object *parser_get_object_attr_value(json_object *obj, char *attr,
					   char *str);
/**
 * parser_get_next_child() - get next object child
 *
 * @jobj : root object
 * @key : object name ptr
 *
 * RETURNS: the next object
 */
json_object *parser_get_next_child(json_object *jobj,  char *key);

/**
 * parser_get_next_array_object() - traverse array
 *
 * @jobj : root array object
 *
 * RETURNS: the next object in the array
 */
json_object *parser_get_next_array_object(json_object *jobj);

/**
 * parser_get_str_key() - get object key
 *
 * @jobj : object
 *
 * RETURNS: the object key
 */
char *parser_get_str_key(json_object *jobj);

#endif /* __parser_h */
