# matrix-cbot makefile

CC=gcc

BIN=mcbot

# libraries
LIBS=-lcbot -ljson-c -lcurl -lolm

library=lib

# relative paths
SRCDIR=src
INCDIR=include
OBJDIR=obj
BINDIR=.

SRCS:=$(wildcard $(SRCDIR)/*.c)
OBJS:=$(patsubst %.c,%.o,$(SRCS))
OBJS:=$(patsubst $(SRCDIR)%,$(OBJDIR)%,$(OBJS))

CFLAGS=-c -pipe -fomit-frame-pointer -Wall -O0 -ggdb -std=c99 -D_POSIX_C_SOURCE=200809L

.PHONY: all $(library)

all: $(BINDIR)/$(BIN)

$(library):
	$(MAKE) --directory=$@

$(BINDIR)/$(BIN): $(OBJS) $(library)
	$(CC) -Llib $(OBJS) $(LIBS) -o $@

$(OBJDIR)/%.o: $(SRCDIR)/%.c | $(OBJDIR)
	$(CC) -I$(INCDIR) -Ilib/include $(CFLAGS) $< -o $@

$(OBJDIR):
	mkdir -p $(OBJDIR)

clean:
	$(MAKE) -C lib clean
	rm $(OBJS)
	rm $(BIN)
