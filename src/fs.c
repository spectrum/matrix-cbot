/*
 * This file is part of the matrix-cbot framework
 *    (https://codeberg.org/spectrum/matrix-cbot).
 *
 * Copyright (C) 2020 Kernelspace <info@kernel-space.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "fs.h"
#include "unistd.h"

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>

int fs_open(char *fname, enum fs_open_mode omode)
{
	int flags = O_CREAT | O_APPEND;

	if (omode == fs_ro)
		flags |= O_RDONLY;
	else if (omode == fs_rw)
		flags |= O_RDWR;

	return open(fname, flags);
}

int fs_file_exists(char *fname)
{
	if (access(fname, F_OK) == 0)
		return 1;

	return 0;
}

int fs_write_str(int file, char *str)
{
	if (file >= 0)
		return write(file, str, strlen(str));

	return -1;
}

int fs_read_line(int file, char *buff)
{
	int c, len = 0;

	if (file >= 0) {
		for (;;) {
			c = read(file, buff, 1);

			if (c == 0 || c == -1)
				return -1;

			if (*buff == '\n' || len == MAX_RD_LINE) {
				*buff = 0;
				return len;
			}
			buff++;
			len++;
		}
	}

	return -1;
}
