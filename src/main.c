/*
 * This file is part of the matrix-cbot framework
 *    (https://codeberg.org/spectrum/matrix-cbot).
 *
 * Copyright (C) 2020 Kernelspace <info@kernel-space.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include <getopt.h>
#include <stdio.h>
#include <getopt.h>
#include <stdlib.h>

#include "version.h"
#include "engine.h"
#include "trace.h"

void usage(void)
{
	printf("Usage: mcbot [OPTION]\n");
	printf("Options:\n");
	printf("  --help     -h    this help\n");
	printf("  --version        version info\n");
}

int main(int argc, char **argv)
{
	int c;

	while (1) {
		static struct option long_options[] = {
			/* These options set a flag. */
			{"help", no_argument, 0, 'h'},
			{"version", no_argument, 0, 'v'},
			{0, 0, 0, 0}
		};

		/* getopt_long stores the option index here. */
		int option_index = 0;

		c = getopt_long(argc, argv, "h", long_options, &option_index);

		/* Detect the end of the options. */
		if (c == -1)
			break;

		switch (c) {
		case 0:
			/* If this option set a flag, do nothing else now. */
			if (long_options[option_index].flag != 0)
				break;
			printf("option %s", long_options[option_index].name);
			if (optarg)
				printf(" with arg %s", optarg);
			break;
		case 'v':
			printf("v." VERSION "\n");
			exit(0);
			break;
		default:
		case 'h':
			usage();
			exit(0);
		}
	}

	inf("mcbot v." VERSION " started.\n");

	return run();
}
