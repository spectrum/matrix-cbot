/*
 * This file is part of the matrix-cbot framework
 *    (https://codeberg.org/spectrum/matrix-cbot).
 *
 * Copyright (C) 2020 Kernelspace <info@kernel-space.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "fs.h"
#include "trace.h"
#include "timestamp.h"

#include <stdio.h>
#include <stdarg.h>
#include <string.h>

#define INF "\x1b[33;1m"
#define ERR "+++ \x1b[31;1m"
#define DBG "d"
#define RST "\x1b[0m"

#define LOG_FILE	"/var/log/mcbot"

static int log_file = -1;
static int cant_log;

/*
 * inf() messages on console
 * log() log to file
 */
static void trace_print(char *hdr, char *fmt, va_list ap)
{
	printf(hdr);
	vprintf(fmt, ap);
	printf(RST);
}

static void trace_to_file(char *fmt, va_list ap)
{
	if (cant_log)
		return;

	if (log_file == -1) {
		log_file = fs_open(LOG_FILE, fs_rw);
		if (log_file == -1) {
			printf("cannot create log file\n");
			cant_log = 1;
			return;
		}
		char start_msg[256];

		time_get_current(start_msg);
		strcat(start_msg, " +++ bot started +++\n");
		fs_write_str(log_file, start_msg);
	}
	vdprintf(log_file, fmt, ap);
}

static void trace(char *hdr, char *fmt, va_list ap)
{
	va_list log;

	va_copy(log, ap);

	trace_print(hdr, fmt, ap);
	trace_to_file(fmt, log);
}

void trace_inf(char *fmt, ...)
{
	va_list ap;

	va_start(ap, fmt);
	trace(INF, fmt, ap);
	va_end(ap);
}

void trace_err(char *fmt, ...)
{
	va_list ap;

	va_start(ap, fmt);
	trace(ERR, fmt, ap);
	va_end(ap);
}

#ifdef DEBUG
void trace_dbg(char *fmt, ...)
{
	va_list ap;

	va_start(ap, fmt);
	trace(DBG, fmt, ap);
	va_end(ap);
}

#else
void trace_dbg(char *fmt, ...) {}
#endif
