/*
 * This file is part of the matrix-cbot framework
 *    (https://codeberg.org/spectrum/matrix-cbot).
 *
 * Copyright (C) 2020 Kernelspace <info@kernel-space.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include <time.h>
#include <unistd.h>
#include <stdlib.h>

#include "rc.h"
#include "net.h"
#include "cipher.h"
#include "trace.h"
#include "parser.h"
#include <string.h>
#include <stdio.h>

#define MAX_RANDOM 512
#define MAX_JSON 4096
#define MAX_TEXT 4096
#define MAX_PATH 512
#define M_ROOM_MEMBER		"m.room.member"
#define M_ROOM_ENCRYPTED	"m.room.encrypted"
#define MEMBERSHIP_JOIN		"join"
extern struct cfg cfg;

static int server;
static struct device_keys dev_keys;
static struct olm_session megolm_session;
static char device_id[MAX_DEVICE_NAME];
static struct device *engine_find_device(struct room_info *ri,
					 char *sender, char *device)
{
	int i, q;

	for (i = 0; i < MAX_ROOM_USERS; ++i) {
		if (!*ri->users[i].id)
			break;
		if (!strcmp(ri->users[i].id, sender)) {
			for (q = 0; q < MAX_DEVICES; ++q) {
				if (!*ri->users[i].devs[q].name)
					break;
				if (!strcmp(ri->users[i].devs[q].name,
					device)) {
					return &ri->users[i].devs[q];
				}
			}
		}
	}
	return NULL;
}

static struct device *engine_find_device_by_curve(struct room_info *ri,
					 char *sender, char *curve)
{
	int i, q;

	for (i = 0; i < MAX_ROOM_USERS; ++i) {
		if (!*ri->users[i].id)
			break;
		if (!strcmp(ri->users[i].id, sender)) {
			for (q = 0; q < MAX_DEVICES; ++q) {
				if (!*ri->users[i].devs[q].name)
					break;
				if (!strcmp(ri->users[i].devs[q].curve25519,
					curve)) {
					return &ri->users[i].devs[q];
				}
			}
		}
	}
	return NULL;
}
static int engine_share_megolm_session(struct room_info *ri, struct user *user)
{
	int q;
	char finalPayload[MAX_JSON];
	char key_request[MAX_JSON];
	char one_time_key[512];
	int ret;

	inf("net_setup_m_room_key, one for all ...\n");
	for (q = 0; q < MAX_DEVICES; ++q) {
		if (!*user->devs[q].name)
			break;

		inf("session id = %08x\n", user->devs[q].s);

		/* Do we have a session id with at least one device ? */
		if (!user->devs[q].s) {
			inf("need outbound olm session for this device ...\n");

			user->devs[q].s = malloc(sizeof(struct olm_session));

			/* Get one-time keys */
			inf("getting one-time keys ...\n");
			if (net_claim_one_time_key(server,
				user->id, user->devs[q].name, one_time_key))
				/*
				 * if any of these conditions is false, then we
				 * proceed for the next one
				 */
				continue;

			/*We need also the device curve25519*/
			if (net_query_curve_key(server,
				user->id, user->devs[q].name, 
user->devs[q].curve25519))
				continue;

			inf("create session ...\n");
			if (cipher_create_outbound_session(
				user->devs[q].curve25519,
				one_time_key, user->devs[q].s))
				continue;


		} else
			return 0;
		net_setup_megolm_key_sharing_payload(ri,
						&megolm_session,
						&(user->devs[q]),
						user, &dev_keys, finalPayload);

		inf("send m_room_encrypted to device: %s\n",
			user->devs[q].name);
		ret = net_send_m_room_encrypted_to_device(server, user->id,
							user->devs[q].name,
							finalPayload);
		/*Sending request for their megolm*
		 *the m.room_key_request is not forwarded
		 * from the server to the client
		 */
		net_request_megolm_keys(ri,
					one_time_key,
					megolm_session.ed25519,
					"oo",
					device_id,
					key_request);
		net_send_to_deivce(server,
				user->id,
				user->devs[q].name,
				key_request,
				"m.room_key_request");
		if (ret != 0)
			return 1;
	}

	return 0;
}

static int engine_share_keys(struct room_info *ri)
{
	int i, q, ret;

	if (net_get_room_members(server, ri))
		return 1;

	inf("room user list:\n");

	for (i = 0; i < MAX_ROOM_USERS; ++i) {
		if (!*ri->users[i].id)
			break;
		inf("%04d: %s\n", i + 1, ri->users[i].id);
	}

	inf("getting devices for room %s ...\n", cfg.room);

	for (i = 0; i < MAX_ROOM_USERS; ++i) {
		if (!*ri->users[i].id)
			break;
		/*We dont need to get our own keys*/
		if (strcmp(ri->users[i].id, cfg.username) == 0)
			continue;
		if (!net_query_device_keys(server, &ri->users[i])) {
			inf("devices for user %s\n", ri->users[i].id);
			for (q = 0; q < MAX_DEVICES; ++q) {
				if (!*ri->users[i].devs[q].name)
					break;
				inf("id: %s\n", ri->users[i].devs[q].name);
				inf("info: %s\n", ri->users[i].devs[q].info);
				inf("key ed25519 (id): %s\n",
				    ri->users[i].devs[q].ed25519);
			}
		}
	}

	inf("sharing megolm session data with all users ...\n");
	for (i = 0; i < MAX_ROOM_USERS; ++i) {
		if (!*ri->users[i].id)
			break;
		inf("sharing megolm session with: %s\n", ri->users[i].id);
		ret = engine_share_megolm_session(ri, &ri->users[i]);
		if (ret != 0)
			inf("error during sharing megolm session with %s\n",
							ri->users[i].id);
	}

	return 0;
}

static void engine_on_new_room_message(struct room_info *ri,
				       struct evt_encrypted *evt)
{
	uint32_t message_index;
	uint8_t plaintext[MAX_JSON] = {0};
	char text[MAX_TEXT];
	struct device *dev;

	dbg("%s\n", __func__);

	dev = engine_find_device(ri, evt->sender, evt->device_id);
	if (!dev)
		return;

	inf("try to devcrypt now ...\n");
	if (!dev->s->inboud)
		return;
	int ret = cipher_decrypt_group((uint8_t *)evt->ciphertext,
			     plaintext, &message_index,
			     dev->s->inboud);
	/*If we weren't able to decrypt, then return*/
	if (ret)
		return;

	printf(">>>> %s\n", plaintext);
#define ECHOBOT 1
#if ECHOBOT
	json_object *jobj = parser_get_root_obj(plaintext);

	parser_get_object_value(jobj, "content.body", text);
	net_send_text_message(text, ri, &megolm_session, server,
							&dev_keys,
							device_id);
#endif
}

static void engine_to_device_event(struct room_info *ri,
				struct to_device *td_event)
{
	OlmSession *session;
	char message[MAX_JSON];
	char message_cp[MAX_JSON] = {0};
	json_object *jobj;
	char *plaintext;
	char path[MAX_PATH] = { 0 };
	ssize_t s_size;
	void *mem;
	int type = 0;
	enum json_type json_type;

	struct evt_encrypted *evt = td_event->content;
	struct device *d = engine_find_device_by_curve(ri,
					      td_event->sender,
					      evt->sender_key);
	/*Once we have the device d, we can get the existing olm_session*/
	if (!d)
		return;

	session = d->s->session;
	jobj = parser_get_root_obj(evt->ciphertext);
	sprintf(path, "%s.body", dev_keys.curve25519);
	parser_get_object_value(jobj, path, message);
	parser_get_object_value(jobj, path, message_cp);
	/*Using this to get type integer*/
	sprintf(path, "%s", dev_keys.curve25519);
	jobj = parser_get_object(jobj, path);
	json_object_object_foreach(jobj, key, val) {
		json_type = json_object_get_type(val);
		if (json_type == json_type_int)
			type = json_object_get_int(val);
	}
	size_t palintext_len = olm_decrypt_max_plaintext_length(session,
								type,
								message,
								strlen(message))
	;
	plaintext = malloc(sizeof(char)*palintext_len);
	if (olm_decrypt(session, type, message_cp, strlen(message_cp),
		    plaintext, palintext_len) == olm_error()) {
		printf("error during olm_decrypt %s\n",
		       olm_session_last_error(session));
		return;
	}
	printf("plaintext %s\n", plaintext);
	/*Now we need to store the megolm key for the device
	 *and start an inboud megolm session
	 */
	jobj = parser_get_root_obj(plaintext);
	parser_get_object_value(jobj,
				"content.session_key",
				d->megolm_session);
	/*start an inbound session*/
	s_size = olm_inbound_group_session_size();

	mem = malloc(s_size);
	if (!s_size)
		return;
	d->s->inboud = olm_inbound_group_session(mem);
	if (!(d->s->inboud))
		return;

	olm_init_inbound_group_session(d->s->inboud,
				       d->megolm_session,
				       strlen(d->megolm_session));

}
static void engine_m_room_member(struct m_room_member *evt,
				 struct room_info *ri)
{
	char message[MAX_TEXT];
	if (strcmp(evt->membership, MEMBERSHIP_JOIN) == 0) {
		engine_share_keys(ri);
		sprintf(message,
			"<b>Welcome to the group %s</b>"
			"<ul>"
			"<li>Please present yourself to others "
			"to avoid being identified as a bot and being "
			"kicked from admins </li>"
			"<li>Please read the description, have a good day "
			":-)</li>"
			"</ul>", evt->sender);
		net_send_text_message(message,
				      ri,
				      &megolm_session,
				      server,
				      &dev_keys,
				      device_id);
	}
}
/* Calling this for each event
 */
static void engine_on_event(json_object *evt,
			    struct room_info *ri)
{
	char type[MAX_TYPE];
	char path[MAX_PATH];
	struct m_room_member *event_member;
	struct evt_encrypted *e;

	parser_get_object_attr_value(evt, "type", type);

	if (strcmp(type, M_ROOM_MEMBER) == 0) {
		/*Now we know what to get from this evt*/
		event_member = malloc(sizeof(struct m_room_member));
		if (!event_member)
			return;
		sprintf(path, "content.membership");
		parser_get_object_value(evt, path,
			event_member->membership);
		sprintf(path, "sender");
		parser_get_object_value(evt, path, event_member->sender);
		engine_m_room_member(event_member, ri);

	} else if (strcmp(type, M_ROOM_ENCRYPTED) == 0) {

		e = malloc(sizeof(struct evt_encrypted));

		if (!parser_get_object_attr_value(evt, "sender",
					e->sender))
			return;
		if (parser_get_object_value(evt, "content.algorithm",
					e->algorithm))
			return;
		if (parser_get_object_value(evt, "content.ciphertext",
					e->ciphertext))
			return;
		if (parser_get_object_value(evt, "content.device_id",
					e->device_id))
			return;
		if (parser_get_object_value(evt, "content.sender_key",
					e->sender_key))
			return;
		if (parser_get_object_value(evt, "content.session_id",
					e->session_id))
			return;
		engine_on_new_room_message(ri, e);
	}
}
int engine(struct room_info *ri)
{
	for (;;) {
		sleep(3);

		net_sync_messages(server, ri,
				  &engine_to_device_event,
				  &engine_on_event);
	}

	return 0;
}

int run(void)
{
	int i, q, ret;
	struct room_info *ri;

	inf("starting engine\n");

	if (rc_load_config())
		exit(1);

	if (net_init()) {
		err("cannot initialize net module\n");
		return 1;
	}

	inf("net module initialized\n");

	if (cipher_init(&megolm_session)) {
		err("cannot initialize cipher module\n");
		return 1;
	}

	if (cipher_create_device_keys(&dev_keys)) {
		err("cannot get device keys\n");
		return 1;
	}

	inf("device key ed25519 (id): %s\n", dev_keys.ed25519);
	inf("device key curve25519 (one-time): %s\n", dev_keys.curve25519);

	inf("megolm session id ed25519 (id): %s\n", megolm_session.ed25519);
	inf("megolm enc_keys: %s\n", megolm_session.session_key);

	server = net_server_login(cfg.server,
				       cfg.username,
				       cfg.password,
				       device_id);
	if (server)
		return 1;

	inf("token received\n");
	inf("device id:%s\n", device_id);
	inf("joining room %s ...\n", cfg.room);

	ri = net_join_room(server, cfg.room);

	if (ri == NULL)
		return 1;

	inf("uploading keys to server...\n");

	char device_keys[1024];
	char *one_time_keys;

	one_time_keys = cipher_get_one_time_keys();
	cipher_get_identity_keys(device_keys, device_id);
	net_upload_keys(device_keys, one_time_keys, "@mcbot:matrix.org",
						device_id, server);

	engine_share_keys(ri);
#define SEND_MESSAGE_TEST 1
#if SEND_MESSAGE_TEST
	inf("sending message to room...\n");
	net_send_text_message("Running...", ri, &megolm_session, server,
				&dev_keys, device_id);
#endif
	return engine(ri);
}
