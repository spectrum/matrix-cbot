/*
 * This file is part of the matrix-cbot framework
 *    (https://codeberg.org/spectrum/matrix-cbot).
 *
 * Copyright (C) 2020 Kernelspace <info@kernel-space.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "rc.h"
#include "fs.h"
#include "trace.h"

#include <string.h>
#include <strings.h>

#define CFG_NAME	".mcbotrc"
#define MAX_RC_LINE	1024

struct cfg cfg;

static int rc_find_config(char *fname)
{
	strncpy(fname, "./", MAX_PATH);
	strncpy(fname, CFG_NAME, MAX_PATH);

	if (fs_file_exists(fname))
		return 0;

	return -1;
}

static char *rc_trim_spaces(char *line)
{
	while (*line == ' ')
		line++;

	return line;
}

static int rc_get_string(char *line, char *dest)
{
	char *p = line;

	while (*line != 0) {
		if (*line == '"' && *(line - 1) == ' ')
			*line = ' ';
		if (*line == '"' && (*(line + 1) == ' ' || *(line + 1) == 0)
			&& *(line - 1) != '\\')
			*line = 0;
		line++;
	}
	p = rc_trim_spaces(p);

	strncpy(dest, p, strlen(p));

	return 0;
}

static int rc_get_var(char *line, char *var, char *dest)
{
	int l = strlen(var);

	if ((strncasecmp(line, var, l) == 0)) {
		if (*(line + l) == ' ') {
			rc_get_string(line + l, dest);
			return 0;
		}
	}

	return 1;
}

static int rc_parse_line(char *line, int len)
{
	char *c;

	/* Discard comments, from # onmward */
	c = strchr(line, '#');

	if (c) {
		char *p1;
		char *p2;

		p1 = strstr(line, " \"");
		if (p1)
			p2 = strchr(p1 + 2, '\"');

		if (p1 && p2) {
			if (c < p1 || c > p2)
				*c = 0;
		} else
			*c = 0;
	}

	/* Skip empty lines */
	if (*line == 0)
		return 0;

	/* Getting first field */
	line = rc_trim_spaces(line);

	if (!rc_get_var(line, "server", cfg.server))
		return 0;
	if (!rc_get_var(line, "username", cfg.username))
		return 0;
	if (!rc_get_var(line, "password", cfg.password))
		return 0;
	if (!rc_get_var(line, "room", cfg.room))
		return 0;

	return 1;
}

int rc_load_config(void)
{
	int rc, i;
	char cfg_file_path[MAX_PATH];
	char line[MAX_RD_LINE];

	if (rc_find_config(cfg_file_path)) {
		err("no rc config file found\n");
		return -1;
	}

	rc = fs_open(cfg_file_path, fs_ro);
	if (rc == -1)
		return 1;

	for (;;) {
		i = fs_read_line(rc, line);
		if (i == 0)
			continue;
		if (i == -1)
			break;

		rc_parse_line(line, i);
	}

	return 0;
}
