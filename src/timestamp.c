/*
 * This file is part of the matrix-cbot framework
 *    (https://codeberg.org/spectrum/matrix-cbot).
 *
 * Copyright (C) 2020 Kernelspace <info@kernel-space.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include <time.h>
#include <stdio.h>

void time_get_current(char *date_time)
{
	time_t t = time(NULL);
	struct tm tm = *localtime(&t);

	sprintf(date_time, "%02d-%02d-%d %02d:%02d:%02d",
		tm.tm_mday,
		tm.tm_mon + 1,
		tm.tm_year + 1900,
		tm.tm_hour,
		tm.tm_min,
		tm.tm_sec);
}