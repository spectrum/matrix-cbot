#ifndef fs_h
#define fs_h

#define MAX_PATH		512
#define MAX_RD_LINE		1024

enum fs_open_mode {
	fs_ro,
	fs_rw,
};

int fs_open(char *fname, enum fs_open_mode);
int fs_file_exists(char *fname);
int fs_write_str(int file, char *str);
int fs_read_line(int file, char *buff);

#endif /* fs_h */
