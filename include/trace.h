#ifndef trace_h
#define trace_h

void trace_inf(char *fmt, ...);
void trace_log(char *fmt, ...);
void trace_err(char *fmt, ...);
void trace_dbg(char *fmt, ...);

#define inf(...) trace_inf(__VA_ARGS__)
#define err(...) trace_err(__VA_ARGS__)
#define log(...) trace_log(__VA_ARGS__)
#define dbg(...) trace_dbg(__VA_ARGS__)

#endif /* trace_h */
