#ifndef rc_h
#define rc_h

#define MAX_LEN_SERVER		512
#define MAX_LEN_USERNAME	64
#define MAX_LEN_PASSWORD	64
#define MAX_ROOM		64

struct cfg {
	char server[MAX_LEN_SERVER];
	char username[MAX_LEN_USERNAME];
	char password[MAX_LEN_PASSWORD];
	char room[MAX_ROOM];
};

int rc_load_config(void);

#endif /* rc_h */
